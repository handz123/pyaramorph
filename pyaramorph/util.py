# util.py
# Helpful functions.

import re
from pkg_resources import resource_filename
from collections import namedtuple

class DictionaryLoadError(Exception):
    """Indicates a problem while loading a dictionary"""

DictionaryEntry = namedtuple("DictionaryEntry",
        "unvoc voc cat gloss POS lemmaID")

RE_POS = re.compile('<pos>(.+?)</pos>')
RE_LEMMA = re.compile('^;; (.*)$')

RE_AZ = re.compile('^[A-Z]')
RE_iy = re.compile('iy~$')

MAX_PREFIX_LENGTH = 4
MAX_SUFFIX_LENGTH = 6

def segment_indexes(wordlen):
    """Generate possible segment indexes.
    
    A word can be divided into three parts: prefix+stem+suffix. The
    prefix and suffix are optional, but the stem is mandatory.

    In this function we generate all the possible ways of breaking down
    a word of the given length. The generator returns a pair of values,
    representing the stem index and the suffix index.
    """
    prelen = 0
    suflen = 0

    while prelen <= MAX_PREFIX_LENGTH:
        stemlen = wordlen - prelen
        suflen = 0

        while stemlen >= 1 and suflen <= MAX_SUFFIX_LENGTH:
            # Cannot have zero-length stem.
            yield (prelen, prelen + stemlen)
            
            stemlen -= 1
            suflen += 1

        prelen += 1

def _data_file_path(filename):
    """Return the path to the given data file."""
    return resource_filename(__name__, filename)

def _parse_POS(cat, voc, gloss):
    """Return the correct category string."""
    if cat.startswith('Pref-0') or cat.startswith('Suff-0'):
        return "" # null prefix or suffix
    elif cat.startswith('F'):
        return "{}/FUNC_WORD".format(voc)
    elif cat.startswith('IV'):
        return "{}/VERB_IMPERFECT".format(voc)
    elif cat.startswith('PV'):
        return "{}/VERB_PERFECT".format(voc)
    elif cat.startswith('CV'):
        return "{}/VERB_IMPERATIVE".format(voc)
    elif cat.startswith('N') and RE_AZ.search(gloss):
        return "{}/NOUN_PROP".format(voc) # educated guess
                # (99% correct)
    elif cat.startswith('N') and RE_iy.search(voc):
        return "{}/NOUN".format(voc) # (was NOUN_ADJ:
                # some of these are really ADJ's
                # and need to be tagged manually)
    elif cat.startswith('N'):
        return "{}/NOUN".format(voc)
    else:
        raise DictionaryLoadError(
                "unknown morphological category {}".format(cat))

def _parse_entry(line, lemmaID):
    """Parse the given entry line for the given lemma and return a
    corresponding DictionaryEntry object."""
    unvoc, voc, cat, gloss = line.strip(' \n').split('\t')

    m = RE_POS.search(gloss)
    if m:
        POS = m.group(1)
    else:
        POS = _parse_POS(cat, voc, gloss)

    gloss = RE_POS.sub('', gloss)
    gloss = gloss.strip()

    return DictionaryEntry(unvoc, voc, cat, gloss, POS, lemmaID)

def load_dict(filename):
    """Load and return the given dictionary."""
    dictionary = {}
    seen = set()
    lemmas = 0
    entries = 0
    lemmaID = ""

    with open(_data_file_path(filename), 'r', encoding='utf-8') as infile:

        print("loading %s ... " % (filename), end='')

        for line in infile:
            if line.startswith(';; '): # a new lemma
                m = RE_LEMMA.match(line)
                lemmaID = m.group(1)
                if lemmaID in seen:
                    raise DictionaryLoadError(
                            "lemmaID %s in %s isn't unique!" % \
                            (lemmaID, filename))
                else:
                    seen.add(lemmaID)
                    lemmas += 1;

            elif line.startswith(';'): # a comment
                continue

            else: # an entry
                entry = _parse_entry(line, lemmaID)
                dictionary.setdefault(entry.unvoc, []).append(entry)
                entries += 1

                # TODO catch and rethrow DictionaryLoadError:
                # "no POS can be deduced in %s: %s" % (filename, line)

    if lemmaID != "":
        print("loaded %d lemmas and %d entries" % (lemmas, entries))
    else:
        print("loaded %d entries" % (entries))
    return dictionary

RE_WSPC = re.compile('\s+')

def load_table(filename):
    """Load and return the given table."""
    table = set()

    with open(_data_file_path(filename), 'r', encoding='utf-8') as infile:

        for line in infile:
            if line.startswith(';'): continue # comment line
            line = line.strip()
            RE_WSPC.sub(' ', line)
            table.add(line)

    return table

